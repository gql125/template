import os

import torch.cuda
from torch.nn import Module
from typing import Dict
from torch import no_grad, load
from torchvision.io import read_image
from torchvision.utils import save_image
from torchvision.transforms import Pad, Resize


def inference_one_dataset(model: Module, source_dir: str, save_dir: str,
                          ckpt_path: str, use_resize: bool, pad_scale: int):
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    ckpt_info = load(ckpt_path, map_location='cpu')
    model.load_state_dict((ckpt_info['model']))
    img_names = os.listdir(source_dir)
    for img_name in img_names:
        model = model.cuda().eval()
        img_path = os.path.join(source_dir, img_name)
        source_img = read_image(img_path) / 255.0
        if use_resize:
            source_img = Resize((256, 256))(source_img)
        else:
            img_h = source_img.shape[1]
            img_w = source_img.shape[2]
            pad_h = (pad_scale - img_h % pad_scale) % pad_scale
            pad_w = (pad_scale - img_w % pad_scale) % pad_scale
            source_img = Pad(padding=[pad_w, pad_h, 0, 0], padding_mode='reflect')(source_img)
        source_img = source_img.unsqueeze(0).cuda()
        with no_grad():
            output_img = model(source_img)
            output_img = output_img.clamp(0, 1).cpu()
        save_image(output_img, os.path.join(save_dir, img_name))
