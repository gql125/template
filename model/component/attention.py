import torch.nn as nn


class CoordinatesPixelAttentionLayer(nn.Module):
    def __init__(self, in_channels, in_size):
        super(CoordinatesPixelAttentionLayer, self).__init__()
        self._h_gap = nn.AdaptiveAvgPool2d((None, 1))
        self._h_conv = nn.Conv2d(in_channels, in_channels, kernel_size=1,
                                 groups=in_channels, bias=False)
        self._v_gap = nn.AdaptiveAvgPool2d((1, None))
        self._v_conv = nn.Conv2d(in_channels, in_channels, kernel_size=1,
                                 groups=in_channels, bias=False)
        self._vh_conv = nn.Conv2d(in_channels, in_channels, kernel_size=3, padding=1,
                                  padding_mode='reflect', groups=in_channels, bias=False)
        self._vh_sig = nn.Sigmoid()
        self._c_gap = nn.AdaptiveAvgPool2d((1, 1))
        self._c_conv = nn.Conv2d(in_channels, in_channels, kernel_size=1, bias=False)
        self._c_sig = nn.Sigmoid()
        self._cvh_conv = nn.Conv2d(in_channels, 1, kernel_size=1, bias=False)
        self._cvh_sig = nn.Sigmoid()

    def forward(self, x):
        # C * 1 * 1
        y_c = self._c_gap(x)
        y_c = self._c_conv(y_c)
        # C * 1 * W
        y_v = self._v_gap(x)
        y_v = self._v_conv(y_v)
        # C * H * 1
        y_h = self._h_gap(x)
        y_h = self._h_conv(y_h)
        # C * H * W
        y_vh = y_h @ y_v
        y_vh = self._vh_conv(y_vh)
        y_c = self._c_sig(y_c)
        y_cvh = y_c * y_vh
        # 1 * H * W
        y_cvh = self._cvh_conv(y_cvh)
        y_cvh = self._cvh_sig(y_cvh)
        return x * y_cvh


if __name__ == '__main__':
    import torch
    x = torch.randn((2, 128, 128, 128))
    model = CoordinatesPixelAttentionLayer(128, 128)
    y = model(x)
    print(y.shape)
